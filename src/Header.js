import React from 'react';
import {
  BrowserRouter as Router,
  Route,
  Link,
  hashHistory
} from 'react-router-dom'


export default class Header extends React.Component {

  render() {
    return (
      <Route path="/">
      <div>
          <div className="page-sidebar">
              <ul className="x-navigation">
                  <li className="xn-logo">
                      <a href="index.html">Joli Admin</a>
                      <a href="#" className="x-navigation-control"></a>
                  </li>
                  <li className="xn-profile">
                      <a href="#" className="profile-mini">
                          <img src="assets/images/users/avatar.jpg" alt="John Doe"/>
                      </a>
                      <div className="profile">
                          <div className="profile-image">
                              <img src="assets/images/users/avatar.jpg" alt="John Doe"/>
                          </div>
                          <div className="profile-data">
                              <div className="profile-data-name">John Doe</div>
                              <div className="profile-data-title">Web Developer/Designer</div>
                          </div>
                          <div className="profile-controls">
                              <a href="pages-profile.html" className="profile-control-left"><span className="fa fa-info"></span></a>
                              <a href="pages-messages.html" className="profile-control-right"><span className="fa fa-envelope"></span></a>
                          </div>
                      </div>
                  </li>
                  <li className="xn-title">Navigation</li>
                  <li className="openli">
                      <Link to="/" ><span className="fa fa-desktop"></span> <span className="xn-text">Dashboard</span></Link>
                  </li>
                  <li className="xn-openable">
                      <a href="#"><span className="fa fa-files-o"></span> <span className="xn-text">Pages</span></a>
                      <ul>
                          <li><Link to="/about" activeClassName="active"><span className="fa fa-desktop"></span> About</Link></li>
                          <li><a href="pages-gallery.html"><span className="fa fa-image"></span> Gallery</a></li>
                          <li><a href="pages-profile.html"><span className="fa fa-user"></span> Profile</a></li>
                          <li><a href="pages-address-book.html"><span className="fa fa-users"></span> Address Book</a></li>
                          <li className="xn-openable">
                              <a href="#"><span className="fa fa-clock-o"></span> Timeline</a>
                              <ul>
                                  <li><a href="pages-timeline.html"><span className="fa fa-align-center"></span> Default</a></li>
                                  <li><a href="pages-timeline-simple.html"><span className="fa fa-align-justify"></span> Full Width</a></li>
                              </ul>
                          </li>
                          <li className="xn-openable">
                              <a href="#"><span className="fa fa-envelope"></span> Mailbox</a>
                              <ul>
                                  <li><a href="pages-mailbox-inbox.html"><span className="fa fa-inbox"></span> Inbox</a></li>
                                  <li><a href="pages-mailbox-message.html"><span className="fa fa-file-text"></span> Message</a></li>
                                  <li><a href="pages-mailbox-compose.html"><span className="fa fa-pencil"></span> Compose</a></li>
                              </ul>
                          </li>
                          <li><a href="pages-messages.html"><span className="fa fa-comments"></span> Messages</a></li>
                          <li><a href="pages-calendar.html"><span className="fa fa-calendar"></span> Calendar</a></li>
                          <li><a href="pages-tasks.html"><span className="fa fa-edit"></span> Tasks</a></li>
                          <li><a href="pages-content-table.html"><span className="fa fa-columns"></span> Content Table</a></li>
                          <li><a href="pages-faq.html"><span className="fa fa-question-circle"></span> FAQ</a></li>
                          <li><a href="pages-search.html"><span className="fa fa-search"></span> Search</a></li>
                          <li className="xn-openable">
                              <a href="#"><span className="fa fa-file"></span> Blog</a>

                              <ul>
                                  <li><a href="pages-blog-list.html"><span className="fa fa-copy"></span> List of Posts</a></li>
                                  <li><a href="pages-blog-post.html"><span className="fa fa-file-o"></span>Single Post</a></li>
                              </ul>
                          </li>
                          <li className="xn-openable">
                              <a href="#"><span className="fa fa-sign-in"></span> Login</a>
                              <ul>
                                  <li><a href="pages-login.html">App Login</a></li>
                                  <li><a href="pages-login-website.html">Website Login</a></li>
                                  <li><a href="pages-login-website-light.html"> Website Login Light</a></li>
                              </ul>
                          </li>
                          <li className="xn-openable">
                              <a href="#"><span className="fa fa-warning"></span> Error Pages</a>
                              <ul>
                                  <li><a href="pages-error-404.html">Error 404 Sample 1</a></li>
                                  <li><a href="pages-error-404-2.html">Error 404 Sample 2</a></li>
                                  <li><a href="pages-error-500.html"> Error 500</a></li>
                              </ul>
                          </li>
                      </ul>
                  </li>
                  <li className="xn-openable">
                      <a href="#"><span className="fa fa-file-text-o"></span> <span className="xn-text">Layouts</span></a>
                      <ul>
                          <li><a href="layout-boxed.html">Boxed</a></li>
                          <li><a href="layout-nav-toggled.html">Navigation Toggled</a></li>
                          <li><a href="layout-nav-top.html">Navigation Top</a></li>
                          <li><a href="layout-nav-right.html">Navigation Right</a></li>
                          <li><a href="layout-nav-top-fixed.html">Top Navigation Fixed</a></li>
                          <li><a href="layout-nav-custom.html">Custom Navigation</a></li>
                          <li><a href="layout-frame-left.html">Frame Left Column</a></li>
                          <li><a href="layout-frame-right.html">Frame Right Column</a></li>
                          <li><a href="layout-search-left.html">Search Left Side</a></li>
                          <li><a href="blank.html">Blank Page</a></li>
                      </ul>
                  </li>
                  <li className="xn-title">Components</li>
                  <li className="xn-openable">
                      <a href="#"><span className="fa fa-cogs"></span> <span className="xn-text">UI Kits</span></a>
                      <ul>
                          <li><a href="ui-widgets.html"><span className="fa fa-heart"></span> Widgets</a></li>
                          <li><a href="ui-elements.html"><span className="fa fa-cogs"></span> Elements</a></li>
                          <li><a href="ui-buttons.html"><span className="fa fa-square-o"></span> Buttons</a></li>
                          <li><a href="ui-panels.html"><span className="fa fa-pencil-square-o"></span> Panels</a></li>
                          <li><a href="ui-icons.html"><span className="fa fa-magic"></span> Icons</a><div className="informer informer-warning">+679</div></li>
                          <li><a href="ui-typography.html"><span className="fa fa-pencil"></span> Typography</a></li>
                          <li><a href="ui-portlet.html"><span className="fa fa-th"></span> Portlet</a></li>
                          <li><a href="ui-sliders.html"><span className="fa fa-arrows-h"></span> Sliders</a></li>
                          <li><a href="ui-alerts-popups.html"><span className="fa fa-warning"></span> Alerts & Popups</a></li>
                          <li><a href="ui-lists.html"><span className="fa fa-list-ul"></span> Lists</a></li>
                          <li><a href="ui-tour.html"><span className="fa fa-random"></span> Tour</a></li>
                      </ul>
                  </li>
                  <li className="xn-openable">
                      <a href="#"><span className="fa fa-pencil"></span> <span className="xn-text">Forms</span></a>
                      <ul>
                          <li>
                              <a href="form-layouts-two-column.html"><span className="fa fa-tasks"></span> Form Layouts</a>
                              <div className="informer informer-danger">New</div>
                              <ul>
                                  <li><a href="form-layouts-one-column.html"><span className="fa fa-align-justify"></span> One Column</a></li>
                                  <li><a href="form-layouts-two-column.html"><span className="fa fa-th-large"></span> Two Column</a></li>
                                  <li><a href="form-layouts-tabbed.html"><span className="fa fa-table"></span> Tabbed</a></li>
                                  <li><a href="form-layouts-separated.html"><span className="fa fa-th-list"></span> Separated Rows</a></li>
                              </ul>
                          </li>
                          <li><a href="form-elements.html"><span className="fa fa-file-text-o"></span> Elements</a></li>
                          <li><a href="form-validation.html"><span className="fa fa-list-alt"></span> Validation</a></li>
                          <li><a href="form-wizards.html"><span className="fa fa-arrow-right"></span> Wizards</a></li>
                          <li><a href="form-editors.html"><span className="fa fa-text-width"></span> WYSIWYG Editors</a></li>
                          <li><a href="form-file-handling.html"><span className="fa fa-floppy-o"></span> File Handling</a></li>
                      </ul>
                  </li>
                  <li className="xn-openable">
                      <a href="tables.html"><span className="fa fa-table"></span> <span className="xn-text">Tables</span></a>
                      <ul>
                          <li><a href="table-basic.html"><span className="fa fa-align-justify"></span> Basic</a></li>
                          <li><a href="table-datatables.html"><span className="fa fa-sort-alpha-desc"></span> Data Tables</a></li>
                          <li><a href="table-export.html"><span className="fa fa-download"></span> Export Tables</a></li>
                      </ul>
                  </li>
                  <li className="xn-openable">
                      <a href="#"><span className="fa fa-bar-chart-o"></span> <span className="xn-text">Charts</span></a>
                      <ul>
                          <li><a href="charts-morris.html"><span className="xn-text">Morris</span></a></li>
                          <li><a href="charts-nvd3.html"><span className="xn-text">NVD3</span></a></li>
                          <li><a href="charts-rickshaw.html"><span className="xn-text">Rickshaw</span></a></li>
                          <li><a href="charts-other.html"><span className="xn-text">Other</span></a></li>
                      </ul>
                  </li>
                  <li>
                      <a href="maps.html"><span className="fa fa-map-marker"></span> <span className="xn-text">Maps</span></a>
                  </li>
                  <li className="xn-openable">
                      <a href="#"><span className="fa fa-sitemap"></span> <span className="xn-text">Navigation Levels</span></a>
                      <ul>
                          <li className="xn-openable">
                              <a href="#">Second Level</a>
                              <ul>
                                  <li className="xn-openable">
                                      <a href="#">Third Level</a>
                                      <ul>
                                          <li className="xn-openable">
                                              <a href="#">Fourth Level</a>
                                              <ul>
                                                  <li><a href="#">Fifth Level</a></li>
                                              </ul>
                                          </li>
                                      </ul>
                                  </li>
                              </ul>
                          </li>
                      </ul>
                  </li>

              </ul>
          </div>
          <div className="page-content">

              <ul className="x-navigation x-navigation-horizontal x-navigation-panel">
                  <li className="xn-icon-button">
                      <a href="#" className="x-navigation-minimize"><span className="fa fa-dedent"></span></a>
                  </li>
                  <li className="xn-search">
                      <form role="form">
                          <input type="text" name="search" placeholder="Search..."/>
                      </form>
                  </li>
                  <li className="xn-icon-button pull-right">
                      <a href="#" className="mb-control" data-box="#mb-signout"><span className="fa fa-sign-out"></span></a>
                  </li>
                  <li className="xn-icon-button pull-right">
                      <a href="#"><span className="fa fa-comments"></span></a>
                      <div className="informer informer-danger">4</div>
                      <div className="panel panel-primary animated zoomIn xn-drop-left xn-panel-dragging">
                          <div className="panel-heading">
                              <h3 className="panel-title"><span className="fa fa-comments"></span> Messages</h3>
                              <div className="pull-right">
                                  <span className="label label-danger">4 new</span>
                              </div>
                          </div>
                          <div className="panel-body list-group list-group-contacts scroll" style={{height: '200px'}}>
                              <a href="#" className="list-group-item">
                                  <div className="list-group-status status-online"></div>
                                  <img src="assets/images/users/user2.jpg" className="pull-left" alt="John Doe"/>
                                  <span className="contacts-title">John Doe</span>
                                  <p>Praesent placerat tellus id augue condimentum</p>
                              </a>
                              <a href="#" className="list-group-item">
                                  <div className="list-group-status status-away"></div>
                                  <img src="assets/images/users/user.jpg" className="pull-left" alt="Dmitry Ivaniuk"/>
                                  <span className="contacts-title">Dmitry Ivaniuk</span>
                                  <p>Donec risus sapien, sagittis et magna quis</p>
                              </a>
                              <a href="#" className="list-group-item">
                                  <div className="list-group-status status-away"></div>
                                  <img src="assets/images/users/user3.jpg" className="pull-left" alt="Nadia Ali"/>
                                  <span className="contacts-title">Nadia Ali</span>
                                  <p>Mauris vel eros ut nunc rhoncus cursus sed</p>
                              </a>
                              <a href="#" className="list-group-item">
                                  <div className="list-group-status status-offline"></div>
                                  <img src="assets/images/users/user6.jpg" className="pull-left" alt="Darth Vader"/>
                                  <span className="contacts-title">Darth Vader</span>
                                  <p>I want my money back!</p>
                              </a>
                          </div>
                          <div className="panel-footer text-center">
                              <a href="pages-messages.html">Show all messages</a>
                          </div>
                      </div>
                  </li>
                  <li className="xn-icon-button pull-right">
                      <a href="#"><span className="fa fa-tasks"></span></a>
                      <div className="informer informer-warning">3</div>
                      <div className="panel panel-primary animated zoomIn xn-drop-left xn-panel-dragging">
                          <div className="panel-heading">
                              <h3 className="panel-title"><span className="fa fa-tasks"></span> Tasks</h3>
                              <div className="pull-right">
                                  <span className="label label-warning">3 active</span>
                              </div>
                          </div>
                          <div className="panel-body list-group scroll" style={{height: '200px'}}>
                              <a className="list-group-item" href="#">
                                  <strong>Phasellus augue arcu, elementum</strong>
                                  <div className="progress progress-small progress-striped active">
                                      <div className="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="50" aria-valuemin="0" aria-valuemax="100" style={{width: '50%'}}>50%</div>
                                  </div>
                                  <small className="text-muted">John Doe, 25 Sep 2014 / 50%</small>
                              </a>
                              <a className="list-group-item" href="#">
                                  <strong>Aenean ac cursus</strong>
                                  <div className="progress progress-small progress-striped active">
                                      <div className="progress-bar progress-bar-warning" role="progressbar" aria-valuenow="80" aria-valuemin="0" aria-valuemax="100" style={{width: '80%'}}>80%</div>
                                  </div>
                                  <small className="text-muted">Dmitry Ivaniuk, 24 Sep 2014 / 80%</small>
                              </a>
                              <a className="list-group-item" href="#">
                                  <strong>Lorem ipsum dolor</strong>
                                  <div className="progress progress-small progress-striped active">
                                      <div className="progress-bar progress-bar-success" role="progressbar" aria-valuenow="95" aria-valuemin="0" aria-valuemax="100" style={{width: '95%'}}>95%</div>
                                  </div>
                                  <small className="text-muted">John Doe, 23 Sep 2014 / 95%</small>
                              </a>
                              <a className="list-group-item" href="#">
                                  <strong>Cras suscipit ac quam at tincidunt.</strong>
                                  <div className="progress progress-small">
                                      <div className="progress-bar" role="progressbar" aria-valuenow="100" aria-valuemin="0" aria-valuemax="100" style={{width: '100%'}}>100%</div>
                                  </div>
                                  <small className="text-muted">John Doe, 21 Sep 2014 /</small><small className="text-success"> Done</small>
                              </a>
                          </div>
                          <div className="panel-footer text-center">
                              <a href="pages-tasks.html">Show all tasks</a>
                          </div>
                      </div>
                  </li>
              </ul>

              <ul className="breadcrumb">
                  <li><a href="#">Home</a></li>
                  <li className="active">Dashboard</li>
              </ul>

              <div className="page-content-wrap">
                  {this.props.children}
              </div>
          </div>
      </div>
      </Route>
    );
  }
}
