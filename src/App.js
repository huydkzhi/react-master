import React from 'react';
import ReactDOM from 'react-dom';
import Home from './Home.js';
import Header from './Header.js';
import About from './About.js'
import Home_true from './Home_true.js'
import {
  BrowserRouter as Router,
  Route,
  Link,
  hashHistory
} from 'react-router-dom'

export default class BasicExample extends React.Component {
  render() {
    return (
      <Router history={hashHistory}>
        <div>
          <Header>
            <Route exact path="/" component={Home}></Route>
            <Route path="/home" component={Home}></Route>
            <Route path="/about" component={About}></Route>
            <Route path="/home_true" component={Home_true}></Route>
          </Header>
        </div>
      </Router>
    );
  }
}
